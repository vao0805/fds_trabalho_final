/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.exceptions.InvalidLanceException;
import Business.exceptions.NoLeilaoFoundException;
import Business.exceptions.NoUserFoundException;
import Persistence.LanceDAO;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.anyObject;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.MockitoAnnotations;

public class LanceServiceTest {

    @Mock
    LanceDAO dao;

    @InjectMocks
    LanceServiceImpl lanceServ;

    Leilao LEILAO_TEST;
    Usuario USUARIO_TEST;
    int LOTE_TEST;

    public LanceServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        LOTE_TEST = 0;

        LEILAO_TEST = new Leilao();
        LEILAO_TEST.setCPF_CNPJ("000000");
        LEILAO_TEST.setLote(LOTE_TEST);
        LEILAO_TEST.setPrecoMinimo(0.0);
        LEILAO_TEST.setStatus("A");
        LEILAO_TEST.setLances(new ArrayList<>());
        
        USUARIO_TEST = new Usuario();
        USUARIO_TEST.setCategoria("L");
        USUARIO_TEST.setEmail("EMAIL");
        USUARIO_TEST.setId(0);
        USUARIO_TEST.setNome("nome");
        USUARIO_TEST.setDocumento("doc");
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testDarLance() throws InvalidLanceException {
        lanceServ.darLance(USUARIO_TEST, LEILAO_TEST, 1f);
        verify(dao, times(1)).darLance(anyObject());
    }

    @Test(expected = InvalidLanceException.class)
    public void testDarLanceInvalido() throws InvalidLanceException {
        lanceServ.darLance(USUARIO_TEST, LEILAO_TEST, -1f);
        verify(dao, times(0)).darLance(anyObject());
    }

    @Test
    public void testRemoverLance() throws InvalidLanceException {
        int lanceId = 1;
        Lance lance = new Lance();
        lance.setLanceId(lanceId);
        lanceServ.removerLance(lance);
        verify(dao, times(1)).removerLance(lanceId);
    }
    
    @Test(expected = InvalidLanceException.class)
    public void testRemoverLanceInvalido() throws InvalidLanceException {
        lanceServ.removerLance(null);
        verify(dao, times(0)).removerLance(anyObject());
    }

    @Test
    public void testBuscarLances() throws NoLeilaoFoundException, NoUserFoundException {
        lanceServ.buscarLancesByLeilao(LEILAO_TEST);
        verify(dao, times(1)).buscarLancesByLeilao(LOTE_TEST);
    }

}
