package UInterface;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaDisplayErro extends JFrame {

    private JPanel contentPane;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    TelaDisplayErro frame = new TelaDisplayErro();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    /**
     * Create the frame.
     */
    JLabel lblAviso;

    public void SetAviso(String aviso) {
        lblAviso.setText(aviso);
    }

    public TelaDisplayErro() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(200, 200, 500, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JButton btnOk = new JButton("FECHAR");
        btnOk.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                SetAviso("");
                dispose();
            }
        });
        btnOk.setBounds(10, 126, 90, 23);
        contentPane.add(btnOk);

        lblAviso = new JLabel("");
        lblAviso.setFont(new Font("Source Code Pro", Font.BOLD, 16));
        lblAviso.setHorizontalAlignment(SwingConstants.CENTER);
        lblAviso.setBounds(0, 11, 380, 138);
        contentPane.add(lblAviso);
    }
}
