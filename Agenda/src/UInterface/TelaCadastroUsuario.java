package UInterface;

import Business.exceptions.InvalidUsuarioException;
import Business.exceptions.UserAlreadyExistsException;
import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

public class TelaCadastroUsuario extends JDialog {

    private UsuarioFacade user;
    private final JPanel contentPanel = new JPanel();
    private JTextField txtNome;
    private JTextField txtCPF;
    private JTextField txtEmail;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        try {
            TelaCadastroUsuario dialog = new TelaCadastroUsuario();
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the dialog.
     */
    public TelaCadastroUsuario() throws ClassNotFoundException {
        user = new UsuarioFacade();
        setBounds(100, 100, 393, 269);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);

        JLabel lblNewLabel = new JLabel("New label");
        lblNewLabel.setBounds(128, 11, 9, 3);
        contentPanel.add(lblNewLabel);

        JLabel lblNewLabel_1 = new JLabel("Nome");
        lblNewLabel_1.setBounds(10, 11, 46, 14);
        contentPanel.add(lblNewLabel_1);

        txtNome = new JTextField();
        txtNome.setBounds(10, 27, 165, 20);
        contentPanel.add(txtNome);
        txtNome.setColumns(10);

        JLabel lblCpfcnpj = new JLabel("CPF/CNPJ");
        lblCpfcnpj.setBounds(10, 58, 86, 14);
        contentPanel.add(lblCpfcnpj);

        JLabel lblEmail = new JLabel("Email");
        lblEmail.setBounds(10, 104, 86, 14);
        contentPanel.add(lblEmail);

        JComboBox cmbCategoria = new JComboBox();
        cmbCategoria.setModel(new DefaultComboBoxModel(new String[]{"Leiloeiro", "Comum"}));
        cmbCategoria.setBounds(230, 27, 120, 20);
        contentPanel.add(cmbCategoria);

        JLabel lblCategoria = new JLabel("Categoria");
        lblCategoria.setBounds(230, 11, 98, 14);
        contentPanel.add(lblCategoria);

        txtCPF = new JTextField();
        txtCPF.setColumns(10);
        txtCPF.setBounds(10, 73, 165, 20);
        contentPanel.add(txtCPF);

        txtEmail = new JTextField();
        txtEmail.setColumns(10);
        txtEmail.setBounds(10, 120, 165, 20);
        contentPanel.add(txtEmail);

        JPanel pntlException = new JPanel();
        pntlException.setBounds(0, 151, 374, 34);
        contentPanel.add(pntlException);
        pntlException.setLayout(null);

        JLabel lblException = new JLabel("");
        lblException.setHorizontalAlignment(SwingConstants.CENTER);
        lblException.setBounds(0, 0, 374, 34);
        pntlException.add(lblException);

        JButton bntOk = new JButton("Cadastrar");
        bntOk.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                String sNome, sCpf, sEmail, sCategoria;
                sCpf = txtCPF.getText();
                sNome = txtNome.getText();
                sEmail = txtEmail.getText();
                sCategoria = cmbCategoria.getSelectedItem().toString();

                switch (sCategoria) {
                    case "Leiloeiro":
                        sCategoria = "L";
                        break;
                    case "Comum":
                        sCategoria = "C";
                        break;
                    default:
                        sCategoria = "";
                        break;

                }

                try {
                    user.criarUsuario(sNome, sCpf, sEmail, sCategoria);
                } catch (UserAlreadyExistsException ex) {
                    JOptionPane.showMessageDialog(TelaCadastroUsuario.this, "Usuário já existe.", "Atenção", JOptionPane.WARNING_MESSAGE);
                } catch (InvalidUsuarioException ex) {
                    JOptionPane.showMessageDialog(TelaCadastroUsuario.this, "Informações inválidas.", "Atenção", JOptionPane.WARNING_MESSAGE);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TelaCadastroUsuario.this, "Erro inesperado ao cadastrar usuário.", "Erro", JOptionPane.ERROR_MESSAGE);
                    ex.printStackTrace();
                }

            }
        });
        bntOk.setBounds(179, 196, 100, 23);
        contentPanel.add(bntOk);

    }
}
