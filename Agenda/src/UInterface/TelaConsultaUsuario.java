package UInterface;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import Business.Usuario;

public class TelaConsultaUsuario extends JFrame
{

  private JPanel contentPane;
  private JTextField txtCPF;

  /**
   * Launch the application.
   */
  public static void main(String[] args)
  {
    EventQueue.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          TelaConsultaUsuario frame = new TelaConsultaUsuario();
          frame.setVisible(true);
        } catch (Exception e)
        {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the frame.
   */
  public TelaConsultaUsuario()
  {
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setBounds(100, 100, 337, 201);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(null);
    
    JLabel lblNewLabel = new JLabel("CPF/CNPJ");
    lblNewLabel.setBounds(10, 26, 337, 14);
    contentPane.add(lblNewLabel);
    
    txtCPF = new JTextField();
    txtCPF.setBounds(10, 52, 225, 20);
    contentPane.add(txtCPF);
    txtCPF.setColumns(10);
    
    JButton bntProcurar = new JButton("Procurar");
    bntProcurar.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) 
      {
        String sCPF;
        sCPF = txtCPF.getText();
        
        try
        {
          UsuarioFacade usuarioFacade = new UsuarioFacade();
          Usuario user = usuarioFacade.buscarUsuario(sCPF);
          TelaDisplayErro erro = new TelaDisplayErro();
          erro.SetAviso("Documento: " + user.getDocumento() + "\n Nome: " +user.getNome() + "\n Email: " + user.getEmail() + "\n Categoria: " + user.getCategoria());
          erro.setVisible(true);        
        }
        catch(Exception e1)
        {
          TelaDisplayErro erro = new TelaDisplayErro();
          erro.SetAviso(e1.getMessage());
          erro.setVisible(true);
        }
      }
    });
    bntProcurar.setBounds(10, 128, 89, 23);
    contentPane.add(bntProcurar);
  }
}
