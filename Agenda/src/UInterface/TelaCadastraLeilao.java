package UInterface;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaCadastraLeilao extends JFrame
{

  private JPanel contentPane;
  private JTextField txtCPF;
  private JTextField txtPreco;
  private JTextField txtLote;
  /**
   * Launch the application.
   */
  public static void main(String[] args)
  {
    EventQueue.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          TelaCadastraLeilao frame = new TelaCadastraLeilao();
          frame.setVisible(true);
        } catch (Exception e)
        {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the frame.
   */
  public TelaCadastraLeilao()
  {
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setBounds(100, 100, 197, 210);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(null);
    
    JLabel lblNewLabel = new JLabel("CNPJ/CPF");
    lblNewLabel.setBounds(10, 11, 58, 14);
    contentPane.add(lblNewLabel);
    
    txtCPF = new JTextField();
    txtCPF.setBounds(10, 25, 86, 20);
    contentPane.add(txtCPF);
    txtCPF.setColumns(10);
    
    JLabel lblPrecoMinimimo = new JLabel("Preco Minimo");
    lblPrecoMinimimo.setBounds(10, 56, 86, 14);
    contentPane.add(lblPrecoMinimimo);
    
    
    txtPreco = new JTextField();
    txtPreco.setColumns(10);
    txtPreco.setBounds(10, 73, 86, 20);
    contentPane.add(txtPreco);
    
    JLabel lblLote = new JLabel("Lote");
    lblPrecoMinimimo.setBounds(10, 88, 86, 14);
    contentPane.add(lblLote);
    
    txtLote = new JTextField();
    txtLote.setBounds(10, 100, 86, 14);
    contentPane.add(txtLote);
    txtLote.setColumns(10);
    
    JButton btnOk = new JButton("Ok");
    btnOk.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        
        String sPrecoMinimo, sDOC;
        Integer sLote;
        sPrecoMinimo = txtPreco.getText();
        sDOC = txtCPF.getText();
        sLote = Integer.parseInt(txtLote.getText());
        
        
        try
        {
          LeilaoFacade leilao = new LeilaoFacade();
          leilao.criarLeilao(sDOC, Double.parseDouble(sPrecoMinimo), sLote);
        }
        catch(Exception e1)
        {
          TelaDisplayErro erro = new TelaDisplayErro();
          erro.SetAviso(e1.getMessage());
          erro.setVisible(true);
        }
      }
    });
    btnOk.setBounds(10, 135, 89, 23);
    contentPane.add(btnOk);
  }
}
