package UInterface;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class TeleGerenciaLance extends JFrame
{

  private JPanel contentPane;
  private JTextField txtCPF;
  private JTextField txtLance;
  private JTextField textField;

  /**
   * Launch the application.
   */
  public static void main(String[] args)
  {
    EventQueue.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          TeleGerenciaLance frame = new TeleGerenciaLance();
          frame.setVisible(true);
        } catch (Exception e)
        {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the frame.
   */
  public TeleGerenciaLance()
  {
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setBounds(100, 100, 305, 222);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(null);
    
    JLabel lblNewLabel = new JLabel("CPF/CNPJ");
    lblNewLabel.setBounds(10, 23, 146, 14);
    contentPane.add(lblNewLabel);
    
    txtCPF = new JTextField();
    txtCPF.setBounds(10, 36, 86, 20);
    contentPane.add(txtCPF);
    txtCPF.setColumns(10);
    
    JLabel lblLance = new JLabel("Lote");
    lblLance.setBounds(10, 67, 146, 14);
    contentPane.add(lblLance);
    
    txtLance = new JTextField();
    txtLance.setColumns(10);
    txtLance.setBounds(10, 82, 86, 20);
    contentPane.add(txtLance);
    
    JLabel lblPreco = new JLabel("Preco");
    lblPreco.setBounds(129, 23, 146, 14);
    contentPane.add(lblPreco);
    
    textField = new JTextField();
    textField.setBounds(129, 36, 86, 20);
    contentPane.add(textField);
    textField.setColumns(10);
    
    JButton btnDarLance = new JButton("Dar Lance");
    btnDarLance.setBounds(7, 143, 89, 23);
    contentPane.add(btnDarLance);
    
    JButton btnRemoverLance = new JButton("Remover Lance");
    btnRemoverLance.setBounds(129, 143, 107, 23);
    contentPane.add(btnRemoverLance);
  }

}
