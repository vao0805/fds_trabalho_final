package UInterface;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

public class TelaPrincipal extends JDialog {

    private final JPanel contentPanel = new JPanel();

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        try {
            TelaPrincipal dialog = new TelaPrincipal();
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the dialog.
     */
    public TelaPrincipal() {
        setTitle("Gerenciador Leilao");
        setBounds(100, 100, 272, 407);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);

        JButton btnCadastrarUsuario = new JButton("Cadastrar Usuario");
        btnCadastrarUsuario.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                try {
                    TelaCadastroUsuario cadastro = new TelaCadastroUsuario();
                    cadastro.setVisible(true);
                } catch (ClassNotFoundException ex) {
                    JOptionPane.showMessageDialog(TelaPrincipal.this, "Erro Fatal ao abrir janela", "Erro", JOptionPane.ERROR_MESSAGE);
                }

            }
        });
        btnCadastrarUsuario.setBounds(36, 64, 188, 34);
        contentPanel.add(btnCadastrarUsuario);

        JButton btnCadastrarLeilaoBens = new JButton("Cadastro Leilao ou Bens");
        btnCadastrarLeilaoBens.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                TelaCadastraLeilao CadastraLeilao = new TelaCadastraLeilao();
                CadastraLeilao.setVisible(true);
            }
        });
        btnCadastrarLeilaoBens.setBounds(36, 110, 188, 34);
        contentPanel.add(btnCadastrarLeilaoBens);

        JButton btnConsultaLeilao = new JButton("Consulta ou Encerra Leilao");
        btnConsultaLeilao.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                TelaConsultaLeilao tela = new TelaConsultaLeilao();
                tela.setVisible(true);
            }

        });
        btnConsultaLeilao.setBounds(36, 155, 188, 34);
        contentPanel.add(btnConsultaLeilao);

        JButton btnLances = new JButton("Gerenciar Lances");
        btnLances.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                TeleGerenciaLance tela = new TeleGerenciaLance();
                tela.setVisible(true);
            }
        });
        btnLances.setBounds(36, 200, 188, 34);
        contentPanel.add(btnLances);

        JButton btnConsultaUsuario = new JButton("Consulta Usuario");
        btnConsultaUsuario.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TelaConsultaUsuario tela = new TelaConsultaUsuario();
                tela.setVisible(true);
            }
        });
        btnConsultaUsuario.setBounds(36, 245, 188, 34);
        contentPanel.add(btnConsultaUsuario);
    }
}
