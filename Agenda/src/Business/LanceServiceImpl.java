/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.exceptions.InvalidLanceException;
import Business.exceptions.NoLeilaoFoundException;
import Business.exceptions.NoUserFoundException;
import Persistence.LanceDAO;
import Persistence.LanceDAOOracle;
import Persistence.LanceTO;
import java.util.ArrayList;
import java.util.List;


public class LanceServiceImpl implements LanceService {

    protected final LanceDAO lanceDAO;
    protected final UsuarioService usuarioService;

    public LanceServiceImpl() throws ClassNotFoundException {
        lanceDAO = new LanceDAOOracle();
        usuarioService = new UsuarioServiceImpl();
    }

    @Override
    public Lance darLance(Usuario usuario, Leilao leilao, Float valor) throws InvalidLanceException {
        if (valor <= 0) {
            throw new InvalidLanceException();
        }

        LanceTO lance = new LanceTO();
        lance.setCpf_cnpj(usuario.getDocumento());
        lance.setLote(leilao.getLote());
        lance.setValor(valor);

        lance = lanceDAO.darLance(lance);

        Lance result = new Lance();
        result.setUsuario(usuario);
        result.setLeilao(leilao);
        result.setValor(valor);
        result.setLanceId(lance.getLanceId());
        return result;
    }

    @Override
    public void removerLance(Lance lance) throws InvalidLanceException {
        if (lance == null || lance.getLanceId() < 0) {
            throw new InvalidLanceException();
        }
        lanceDAO.removerLance(lance.getLanceId());
    }

    @Override
    public List<Lance> buscarLancesByLeilao(Leilao leilao) throws NoLeilaoFoundException, NoUserFoundException {
        if (leilao.getLote() == null || leilao.getLote() < 0) {
            throw new NoLeilaoFoundException();
        }
        List<LanceTO> lances = lanceDAO.buscarLancesByLeilao(leilao.getLote());
        List<Lance> result = new ArrayList<>();
        for (LanceTO to : lances) {
            Lance lance = new Lance();

            lance.setLanceId(to.getLanceId());
            lance.setLeilao(leilao);
            lance.setUsuario(usuarioService.buscaUsuarioByDoc(to.getCpf_cnpj())); //subsitui o buscaUsuarioById, que recebia o id como Integer, por buscaUsuarioByDoc convertendo, o id dele pra String
            lance.setValor(to.getValor());

            result.add(lance);
        }
        return result;
    }

}
