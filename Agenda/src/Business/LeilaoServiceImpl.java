/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.exceptions.PersistenceException;
import Business.exceptions.InvalidLanceException;
import Business.exceptions.NoUserFoundException;
import Business.exceptions.LeilaoEncerradoException;
import Business.exceptions.NoLeilaoFoundException;
import Persistence.LeilaoDAO;
import Persistence.LeilaoDAOOracle;
import Persistence.LeilaoTO;
import java.util.List;


public class LeilaoServiceImpl implements LeilaoService {

    protected final LeilaoDAO leilaoDAO;

    public LeilaoServiceImpl() throws ClassNotFoundException {
        leilaoDAO = new LeilaoDAOOracle();
    }

    @Override
    public Leilao gerarLeilao(Leilao leilao) throws InvalidLanceException, NoUserFoundException {
        if (leilao.getPrecoMinimo() == null || leilao.getPrecoMinimo() < 0) {
            throw new InvalidLanceException();
        }
        if (leilao.getCriador() == null) {
            throw new NoUserFoundException();
        }

        LeilaoTO to = new LeilaoTO();
        to.setStatus("Aberto");
        to.setPrecoMinimo(leilao.getPrecoMinimo());
        to.setCPF_CNPJ(leilao.getCPF_CNPJ());
        to.setLote(leilao.getLote());
        to = leilaoDAO.criar(to);

        leilao.setLote(to.getLote());
        return leilao;
    }

    @Override
    public void encerrarLeilao(Leilao leilao) throws LeilaoEncerradoException, PersistenceException {
        if ("Aberto'".equalsIgnoreCase(leilao.getStatus())) {
            LeilaoTO to = leilaoDAO.buscaByLote(leilao.getLote());
            try {
                leilaoDAO.encerrar(to);
            } catch (Exception ex) {
                throw new PersistenceException(ex);
            }
        } else {
            throw new LeilaoEncerradoException();
        }
    }
    
    @Override
    public Leilao buscarLeilaoByLote(Integer lote) throws NoLeilaoFoundException {
        if (lote == null || lote <= 0) {
            throw new NoLeilaoFoundException();
        }
        LeilaoTO to = leilaoDAO.buscaByLote(lote);
        if (to == null) {
            throw new NoLeilaoFoundException();
        }
        return toLeilao(to);
    }

    @Override
    public List<Leilao> buscarLeiloesAbertos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Leilao> buscarLeiloesEncerrados() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void encerraLeilaoPorLote(int lote) throws LeilaoEncerradoException, PersistenceException
    {
      LeilaoTO leilao = leilaoDAO.buscaByLote(lote);
      if ("Aberto".equalsIgnoreCase(leilao.getStatus().toString()))
      {     
       try 
        {
            leilaoDAO.encerrar(leilao);
        } catch (Exception ex) {
            throw new PersistenceException(ex);
        }
      } 
      else 
      {
        throw new LeilaoEncerradoException();
      }
    }
    private Leilao toLeilao(LeilaoTO to) {
        Leilao result = new Leilao();
        result.setLote(to.getLote());
        result.setPrecoMinimo(to.getPrecoMinimo());
        result.setStatus(to.getStatus());
        return result;
    }

}
