/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.exceptions.NoUserFoundException;
import Business.exceptions.UserAlreadyExistsException;
import Persistence.UsuarioDAO;
import Persistence.UsuarioDAOOracle;
import Persistence.UsuarioTO;
import java.sql.SQLException;

public class UsuarioServiceImpl implements UsuarioService {

    protected final UsuarioDAO usuarioDAO;

    public UsuarioServiceImpl() throws ClassNotFoundException {
        usuarioDAO = new UsuarioDAOOracle();
    }

    @Override
    public Usuario buscaUsuarioByDoc(String documento) throws NoUserFoundException {
        UsuarioTO to = usuarioDAO.buscarUsuario(documento);
        return toUsuario(to);
    }

    @Override
    public Usuario cadastrar(Usuario novoUsuario) throws UserAlreadyExistsException, SQLException {
        UsuarioTO to = new UsuarioTO();
        to.setDocumento(novoUsuario.getDocumento());
        to.setEmail(novoUsuario.getEmail());
        to.setNome(novoUsuario.getNome());
        to.setCategoria(novoUsuario.getCategoria());

        try {
            UsuarioTO result = usuarioDAO.cadastrar(to);
            novoUsuario.setId(result.getId());
        } catch (SQLException ex) {
            throw ex;
        }

        return novoUsuario;
    }

    /*@Override
    public Usuario buscaUsuarioById(Integer id) throws NoUserFoundException {
        UsuarioTO to = usuarioDAO.buscarUsuarioById(id);
        return toUsuario(to);
    }
     */ //acho que pode ter a busca só pelo doc
    private Usuario toUsuario(UsuarioTO to) throws NoUserFoundException {
        if (to == null) {
            throw new NoUserFoundException();
        }
        Usuario result = new Usuario();
        result.setDocumento(to.getDocumento());
        result.setEmail(to.getEmail());
        result.setId(to.getId());
        result.setNome(to.getNome());
        return result;
    }

}
