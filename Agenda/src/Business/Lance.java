/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

public class Lance implements Comparable<Lance> {

    private Integer lanceId;
    private Usuario usuario;
    private Leilao leilao;
    private Float valor;
    private String cpf_cnpj;

    @Override
    public int compareTo(Lance lance) {
        int compareTo = this.valor.compareTo(lance.getValor());
        if (compareTo == 0)
            return lanceId.compareTo(lance.getLanceId());
        else
            return compareTo;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario user) {
        this.usuario = user;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Float getValor() {
        return valor;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    public Integer getLanceId() {
        return lanceId;
    }

    public void setLanceId(Integer lanceId) {
        this.lanceId = lanceId;
    }    
}
