/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.exceptions.InvalidLanceException;
import Business.exceptions.NoLeilaoFoundException;
import Business.exceptions.NoUserFoundException;
import java.util.List;


public interface LanceService {

    Lance darLance(Usuario usuario, Leilao leilao, Float valor) throws InvalidLanceException;

    void removerLance(Lance lance) throws InvalidLanceException;

    List<Lance> buscarLancesByLeilao(Leilao leilao) throws NoLeilaoFoundException, NoUserFoundException;

}
