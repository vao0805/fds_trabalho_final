/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

public interface LeilaoDAO {

    LeilaoTO criar(LeilaoTO leilao);
    LeilaoTO encerrar(LeilaoTO leilao) throws Exception;
    LeilaoTO buscaByLote(Integer lote);
    void remover(Integer lote);
    
    
}
