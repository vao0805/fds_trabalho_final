/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LeilaoDAOOracle implements LeilaoDAO {


    @Override
    public LeilaoTO criar(LeilaoTO leilao) {
        String doc = null, status = null;
        Double precoMin = null;
        Integer lote = null;
   
        try {
            String user = "";
            String password = "";		
            Class.forName("oracle.jdbc.driver.OracleDriver");		
			
            Connection con = DriverManager.getConnection(
							  "jdbc:oracle:thin:@localhost:1521:xe", user, password);
            String sql = "insert into LEILAO"
                + "(CPF_CNPJ, Preco_min, Status, Lote) "
                + "values (?, ?, Aberto, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
            doc = rs.getString("CPF_CNPJ");
            status = rs.getString("Status");
            precoMin = rs.getDouble("Preco_min");
            lote = rs.getInt("Lote");
            System.out.println("Documento: " + doc + " Status: " + status  + " Email: " + precoMin + " Categoria: " + lote);
            stmt.setString(1, leilao.getCPF_CNPJ());
            stmt.setDouble(2, leilao.getPrecoMinimo());
            stmt.setString(3, leilao.getStatus());
            stmt.setInt(4, leilao.getLote());
            stmt.execute();
            stmt.close();
            return leilao;
        }
       
    }   catch (SQLException ex) {
            Logger.getLogger(LeilaoDAOOracle.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LeilaoDAOOracle.class.getName()).log(Level.SEVERE, null, ex);
        }
         return null;
    }

    @Override
    public LeilaoTO encerrar(LeilaoTO leilao) throws Exception {
        Integer lote = leilao.getLote();
        String user = "";
        String password = "";		
        Class.forName("oracle.jdbc.driver.OracleDriver");		
	Connection con = DriverManager.getConnection(
							  "jdbc:oracle:thin:@localhost:1521:xe", user, password);
        String sql = "UPDATE LEILAO set STATUS = 'Fechado' where Lote = ?";
        try {
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, lote);
            ResultSet rs = stmt.executeQuery();
            System.out.println("Leilao de lote " + lote + " encerrado!");
            leilao.setStatus("Fechado");
            rs.close();
            stmt.close();
            return leilao;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public LeilaoTO buscaByLote(Integer lote) {

        try {
            LeilaoTO leilao = new LeilaoTO();
            Double preco_min = null;
            String status = null;
            String cpf_cnpj = null;
            String user = ""; //colocar o usuário do banco utilizado
            String password = ""; //colocar a senha do banco utilizado
            Class.forName("oracle.jdbc.driver.OracleDriver");	
            Connection con = DriverManager.getConnection(
                     "jdbc:oracle:thin:@localhost:1521:xe", user, password);
            String sql = "select Preco_min, Status, CPF_CNPJ from LEILAO where lote = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, lote);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                preco_min = rs.getDouble("Preco_min");
                status = rs.getString("Status");
                cpf_cnpj = rs.getString("CPF_CNPJ");
                System.out.println(lote + " " + preco_min + " " + status + " " + cpf_cnpj);
            }
            leilao.setLote(lote);
            leilao.setPrecoMinimo(preco_min);
            leilao.setStatus(status);
            leilao.setCPF_CNPJ(cpf_cnpj);
            rs.close();
            stmt.close();
            return leilao;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LeilaoDAOOracle.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
//Esse método tem que retornar o Leilao TO do lote passado por parâmetro
    }

    @Override
    public void remover(Integer lote) {
        Scanner in = new Scanner(System.in);
        System.out.print("Informe o lote do leilão: ");
        lote = in.nextInt();

        String sql = "DELETE from LEILAO where lote = ?";
        try {
            PreparedStatement stmt = this.connection.prepareStatement(sql);
            stmt.setInt(1, lote);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                System.out.println("Leilão deletado com sucesso");
            }
            rs.close();
            stmt.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
