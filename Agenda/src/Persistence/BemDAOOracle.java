/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class BemDAOOracle implements BemDAO{
   
    
    @Override
    public void adicionar(BemTO bem) {
        String sql = "insert into BENS" +
				"(Desc_breve, Categoria, Desc_completa, Lote) "+
				"values (?, ?, ?, ?)";
		try {
			PreparedStatement stmt = this.connection.prepareStatement(sql);
			stmt.setString(1, bem.getDesc_breve());
			stmt.setString(2, bem.getCategoria());
                        stmt.setString(3, bem.getDesc_completa());
			stmt.setInt(4, bem.getLote());
			stmt.execute();
			stmt.close();
		 
		} catch (SQLException e) {
			e.printStackTrace();					
		}
    }

    @Override
    public void remover(BemTO bem) {
       
         Integer lote = bem.getLote();
         String sql = "DELETE from BENS where Lote = ?";
        try {
            PreparedStatement stmt = this.connection.prepareStatement(sql);
            stmt.setInt(1, lote);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                System.out.println("Bem deletado com sucesso");
            }
            rs.close();
            stmt.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }  

    @Override
    public List<BemTO> buscarBensByLeilao(Integer lote) { //verificar
        ArrayList<BemTO> bens = new ArrayList<BemTO>();
        String sql = "SELECT Desc_breve from Bens inner join Leilao on Bens.lote = Leilao.lote;";
        try {
            PreparedStatement stmt = this.connection.prepareStatement(sql);
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                String descBreve = result.getString("Desc_breve");
                bens.add(new BemTO(descBreve));
            }
            return bens;
        } catch (SQLException e) {
            e.printStackTrace();
        }
      return null;
    }
    
}
