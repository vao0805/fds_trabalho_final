/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

public class LeilaoTO {

    private String CPF_CNPJ;
    private Double precoMinimo;
    private Integer lote;
    private String status;

    /**
     * @return the lote
     */
    public Integer getLote() {
        return lote;
    }

    /**
     * @param lote the lote to set
     */
    public void setLote(Integer lote) {
        this.lote = lote;
    }

    /**
     * @return the precoMinimo
     */
    public Double getPrecoMinimo() {
        return precoMinimo;
    }

    /**
     * @param precoMinimo the precoMinimo to set
     */
    public void setPrecoMinimo(Double precoMinimo) {
        this.precoMinimo = precoMinimo;
    }

    /**
     * @return the usuarioId
     */

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCPF_CNPJ() {
        return CPF_CNPJ;
    }

    public void setCPF_CNPJ(String CPF_CNPJ) {
        this.CPF_CNPJ = CPF_CNPJ;
    }
}
