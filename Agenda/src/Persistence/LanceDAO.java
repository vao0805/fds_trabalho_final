/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

import java.util.List;

public interface LanceDAO {
    
    LanceTO darLance(LanceTO lance);
    void removerLance(Integer lanceId);
    List<LanceTO> buscarLancesByLeilao(Integer lote);
}
