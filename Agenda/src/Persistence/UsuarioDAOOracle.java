/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UsuarioDAOOracle implements UsuarioDAO {


    @Override
    public UsuarioTO cadastrar(UsuarioTO usuario) {

        try {
    		String user = "";
		String password = "";		
                Class.forName("oracle.jdbc.driver.OracleDriver");		
			
		Connection con = DriverManager.getConnection(
							  "jdbc:oracle:thin:@localhost:1521:xe", user, password);
            String sql = "insert into USUARIO"
                + "(cpf_cnpj, nome, email, categoria)"
                + "values (?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, usuario.getDocumento());
            stmt.setString(2, usuario.getNome());
            stmt.setString(3, usuario.getEmail());
            stmt.setString(4, usuario.getCategoria());
            stmt.execute();
            stmt.close();
            return usuario;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UsuarioDAOOracle.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /* @Override
    public UsuarioTO atualizar(UsuarioTO usuario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     */
    @Override
    public UsuarioTO buscarUsuario(String documento) {
        String nome = null;
        String email = null;
        String categoria = null;
        try {
            UsuarioTO usuario = new UsuarioTO();
            String user = "";
            String password = "";		
            Class.forName("oracle.jdbc.driver.OracleDriver");		
			
            Connection con = DriverManager.getConnection(
							  "jdbc:oracle:thin:@localhost:1521:xe", user, password);
            /*String user = ""; //colocar o usuário do banco utilizado
            String password = ""; //colocar a senha do banco utilizado	
 Connection con = DriverManager.getConnection(
                    "jdbc:oracle:thin:@//camburi.pucrs.br:1521/facin11g", user, password);
           
*/ 
             String sql = "select CPF_CNPJ, Nome, Email, Categoria from USUARIO where CPF_CNPJ = ?";

            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, documento);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                nome = rs.getString("Nome");
                email = rs.getString("Email");
                categoria = rs.getString("Categoria");
                System.out.println("Documento: " + documento + " Nome: " + nome  + " Email: " + email + " Categoria: " + categoria);
            }
            usuario.setDocumento(documento);
            usuario.setNome(nome);
            usuario.setEmail(email);
            usuario.setCategoria(categoria);
            rs.close();
            stmt.close();
            return usuario;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UsuarioDAOOracle.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
