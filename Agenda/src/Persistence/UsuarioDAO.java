/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

import java.sql.SQLException;

/**
 *
 * @author 15280212
 */
public interface UsuarioDAO {
    
    UsuarioTO cadastrar(UsuarioTO usuario) throws SQLException;
    //UsuarioTO atualizar(UsuarioTO usuario);  Acho que não precisamos dessa opção, não achei ela no enunciado ao menos
    UsuarioTO buscarUsuario(String documento);
}
